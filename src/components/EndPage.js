import React from 'react';
import PropTypes from 'prop-types';
import './EndPage.css'
import {CSSTransitionGroup} from "react-transition-group";
function EndPage(props) {
  return(
    <CSSTransitionGroup
      component="div"
      transitionName="fade"
      transitionEnterTimeout={2500}
      transitionLeaveTimeout={2500}
      transitionAppear
      transitionAppearTimeout={2500}
    >
      <div className="align_center"><p className="text_animate">Congratulation</p></div>
      <div className="access_code"><p>AccessCode: {props.accesscode}</p></div>
    </CSSTransitionGroup>);

}

EndPage.propTypes = {
  accesscode: PropTypes.string.isRequired
};

export default EndPage;
