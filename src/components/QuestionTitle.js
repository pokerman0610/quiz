import React from 'react';
import PropTypes from 'prop-types';

function QuestionTitle(props) {
  return <h2 className="question">{props.content}</h2>;
}

QuestionTitle.propTypes = {
  content: PropTypes.string.isRequired
};

export default QuestionTitle;
