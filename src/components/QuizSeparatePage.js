import React from 'react';
import PropTypes from 'prop-types';
import {Button} from "./styled";
import './QuizSeparatePage.css'
import { CSSTransitionGroup } from 'react-transition-group';
function QuizSeparatePage(props) {
  return(
      <CSSTransitionGroup
          className="container"
          component="div"
          transitionName="fade"
          transitionEnterTimeout={800}
          transitionLeaveTimeout={500}
          transitionAppear
          transitionAppearTimeout={500}
      >
          <div key={props.questionId}>
              <div className="separate_section">
                <div>
                    <h2>Thank you for your try in "{props.quizTitle}"</h2>
                </div>
                <Button className="btn_next" onClick={props.onNextButtonClick}>Next</Button>
              </div>
          </div>
      </CSSTransitionGroup>
  );
}

QuizSeparatePage.propTypes = {
  quizTitle:PropTypes.string.isRequired,
};

export default QuizSeparatePage;
