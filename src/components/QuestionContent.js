import React from 'react';
import PropTypes from 'prop-types';
import AnswerOption from "./AnswerOption";

function QuestionContent(props) {
    function renderAnswerOptions(key) {
        return (
            <AnswerOption
                key={key.content}
                answerContent={key.content}
                answerType={key.type}
                answer={props.answer}
                questionId={props.questionId}
                onAnswerSelected={props.onAnswerSelected}
            />
        );
    }
    return (
      <ul className="answerOptions">
          {props.answerOptions.map(renderAnswerOptions)}
      </ul>
    );
}

QuestionContent.propTypes = {
  // content: PropTypes.string.isRequired
};

export default QuestionContent;
