import React from 'react';
import PropTypes from 'prop-types';
import './QuizTitle.css'
import {CSSTransitionGroup} from "react-transition-group";
function QuizTitle(props) {
  return(
    <CSSTransitionGroup
      component="div"
      transitionName="fade"
      transitionEnterTimeout={2500}
      transitionLeaveTimeout={2500}
      transitionAppear
      transitionAppearTimeout={2500}
    >
      <div className="align_center"><p className="text_animate">{props.Title}</p></div>
    </CSSTransitionGroup>);

}

QuizTitle.propTypes = {
  Title: PropTypes.string.isRequired
};

export default QuizTitle;
