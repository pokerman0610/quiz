import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransitionGroup } from 'react-transition-group';
import QuestionTitle from './QuestionTitle';
import QuestionCount from './QuestionCount';
import QuestionContent from './QuestionContent';
import QuestionResponse from './QuestionResponse';
function Quiz(props) {

  return (
    <CSSTransitionGroup
      className="container"
      component="div"
      transitionName="fade"
      transitionEnterTimeout={800}
      transitionLeaveTimeout={500}
      transitionAppear
      transitionAppearTimeout={500}
    >
      <div key={props.questionId}>
        <QuestionCount counter={props.questionId} total={props.questionTotal} />
        <QuestionTitle content={props.question} />
        <QuestionContent answer={props.answer}
                           answerOptions={props.answerOptions}
                           questionId={props.questionId}
                           question={props.question}
                           onAnswerSelected={props.onAnswerSelected}/>
          {(() => {
              if(props.answer !== ''){
                  return(<QuestionResponse
                      questionId={props.questionId}
                      questionResult={props.questionResult}
                      questionResult={props.questionResult}
                      incorrectAnswer={props.incorrectAnswer}
                      onPrevButtonClick={props.onPrevButtonClick}
                      onNextButtonClick={props.onNextButtonClick}
                  />)
              }
          })()}
      </div>
    </CSSTransitionGroup>
  );
}

Quiz.propTypes = {
  answer: PropTypes.string.isRequired,
  answerOptions: PropTypes.array.isRequired,
  question: PropTypes.string.isRequired,
  questionId: PropTypes.number.isRequired,
  questionTotal: PropTypes.number.isRequired,
  questionResult:PropTypes.string.isRequired,
  incorrectAnswer:PropTypes.string.isRequired,
  onAnswerSelected: PropTypes.func.isRequired,
  onPrevButtonClick: PropTypes.func.isRequired,
  onNextButtonClick: PropTypes.func.isRequired
};

export default Quiz;
