import styled from 'styled-components'

export const IntroWrapper = styled.div `
    max-width: 66%;
    text-align: center;
  `;

export const Header = styled.h1 `
    font-size: 3rem;
    text-align: center;
    font-family: 'InterFace-Bold'
  `;

export const IntroText = styled.p `
    text-align: center;
  `;

export const Button = styled.button `
    border: 1px solid #ff5000;
    background-color: #ff5100;
    border-radius: 5px;
    font-size: 1.8rem;
    color: #efefef;
    font-family: InterFace-Bold;
    padding: 9px 12px;
    cursor: pointer;
  `;
