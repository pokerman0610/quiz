import React from 'react';
import PropTypes from 'prop-types';
import {Button} from "./styled";
import './QuestionResponse.css'
import { CSSTransitionGroup } from 'react-transition-group';
function QuestionResponse(props) {
  return(
      <CSSTransitionGroup
          component="div"
          transitionName="fade"
          transitionEnterTimeout={800}
          transitionLeaveTimeout={500}
          transitionAppear
          transitionAppearTimeout={500}
      >
          <div className="answer_section">
            <div>
                <h2 className="question">{props.questionResult}</h2>
            </div>
            <div className="comment">
              <a>{props.incorrectAnswer}</a>
            </div>
            <div>
                {props.questionId != 1 &&
                    <Button className="btn_prev" onClick={props.onPrevButtonClick}>Prev</Button>
                }
                <Button className="btn_next" onClick={props.onNextButtonClick}>Next</Button>
            </div>
          </div>
      </CSSTransitionGroup>
  );
}

QuestionResponse.propTypes = {
  questionId:PropTypes.number.isRequired,
  questionResult:PropTypes.string.isRequired,
  incorrectAnswer:PropTypes.string.isRequired,
};

export default QuestionResponse;
