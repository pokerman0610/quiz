var quizQuestions =[
    {
        index: 1,
        title: "What is The Access Platform",
        video_title: "WHAT IS TAP?",
        video: "quiz1.mp4",
        quiz: [
            {
                index: 1,
                question: "What is the purpose of The Access Platform?",
                answers: [
                    {
                        type: "A",
                        content: "To break down social and geographical barriers"
                    },
                    {
                        type: "B",
                        content: "To help students make informed and confident decisions about their education"
                    },
                    {
                        type: "C",
                        content: "To provide authentic answers to frequently asked questions"
                    },
                    {
                        type: "D",
                        content: "All of the above"
                    }
                ],
                RightAnswer: "D",
                IncorrectAnswer: "The correct answer is ​D ​because The Access Platform’s purpose is to help with all of the options listed."
            },
            {
                index: 2,
                question: "Where will your profile card appear?",
                answers: [
                    {
                        type: "A",
                        content: "On your institution’s social media channels"
                    },
                    {
                        type: "B",
                        content: "On your institution’s website"
                    },
                    {
                        type: "C",
                        content: "In the app"
                    }
                ],
                RightAnswer: "B",
                IncorrectAnswer: "The correct answer is​ B​: your profile card will appear with your fellow Ambassadors, Staff and Alumni on your institution’s website, so students can ask you questions after reading your profile. "
            },
            {
                question: "How can you earn Career Points?",
                answers: [
                    {
                        type: "A",
                        content: "Replying to messages and submitting content"
                    },
                    {
                        type: "B",
                        content: "Creating a profile"
                    },
                    {
                        type: "C",
                        content: "Spending over 20 minutes per day answering FAQs"
                    }
                ],
                RightAnswer: "A",
                IncorrectAnswer: "The correct answer is ​A:​ you can earn career points by responding to prospective students, submitting content to content groups and answering FAQs. You also get bonus points if your content or answers are published, or if you’re rated well on your chat conversation."
            },
        ],
    },
    {
        index: 2,
        title: " What is Chat?",
        video_title: "WHAT IS CHAT?",
        video: "quiz2.mp4",
        quiz: [
            {
                index: 1,
                question: "What do you do when a conversation causes you concern?",
                answers: [
                    {
                        type: "A",
                        content: "Add another ambassador to take over"
                    },
                    {
                        type: "B",
                        content: "‘Flag’ the conversation to an Admin"
                    },
                    {
                        type: "C",
                        content: "Close the conversation"
                    }
                ],
                RightAnswer: "B",
                IncorrectAnswer: "The correct answer is ​B: ​if a conversation becomes inappropriate and you need help from an admin, you can flag the conversation in the top right of the conversation."
            },
            {
                index: 2,
                question: "If you don’t know the answer to a question, what should you do?",
                answers: [
                    {
                        type: "A",
                        content: "Direct them to a page on your institution’s website"
                    },
                    {
                        type: "B",
                        content: "Say you don’t know and close the conversation"
                    },
                    {
                        type: "C",
                        content: "Add an Admin or fellow Ambassador to the conversation"
                    }
                ],
                RightAnswer: "C",
                IncorrectAnswer: "The correct answer is​ C:​ if you’re unsure of the answer to a prospective student’s question, don’t guess, just add in a fellow Ambassador or an Admin who is likely to know the answer."
            },
            {
                index: 3,
                question: "What happens when you close a conversation after it’s reached its natural conclusion?",
                answers: [
                    {
                        type: "A",
                        content: "Your Admin gives you feedback"
                    },
                    {
                        type: "B",
                        content: "The prospective student gives you feedback"
                    },
                    {
                        type: "C",
                        content: "Your fellow Ambassadors give you feedback"
                    }
                ],
                RightAnswer: "B",
                IncorrectAnswer: " The correct answer is​ B:​ once you’ve finished a conversation a prospective student will provide feedback on how helpful they found the conversation"
            },
        ],
    },
    {
        index: 3,
        title: "What is Content?",
        video_title: "WHAT IS CONTENT?",
        video: "quiz3.mp4",
        quiz: [
            {
                index: 1,
                question: "Where can you send content to your Admin?",
                answers: [
                    {
                        type: "A",
                        content: "In an email"
                    },
                    {
                        type: "B",
                        content: "In a content group or on the feed"
                    },
                    {
                        type: "C",
                        content: "In a Facebook message"
                    }
                ],
                RightAnswer: "B",
                IncorrectAnswer: "The correct answer is ​B​: you’ll be added to a content group where you can send content directly to the Admin, or you can upload it directly to the feed."
            },
            {
                index: 2,
                question: "Once you’ve created the content, where will it go first?",
                answers: [
                    {
                        type: "A",
                        content: "To the in-app Feed and Admin dashboard"
                    },
                    {
                        type: "B",
                        content: "To social media"
                    },
                    {
                        type: "C",
                        content: "To the institution’s website"
                    }
                ],
                RightAnswer: "A",
                IncorrectAnswer: "The correct answer is ​A​: content you create will appear in your in-app Feed and will be sent to the Admin dashboard before it can be approved and shared on the website or social media."
            },
            {
                index: 3,
                question: "When your content has been published, where might you be able to see it?",
                answers: [
                    {
                        type: "A",
                        content: "Only in the app"
                    },
                    {
                        type: "B",
                        content: "On the website and social media"
                    },
                    {
                        type: "C",
                        content: "On your institution’s website, on your profile on the website and/or on social media"
                    }
                ],
                RightAnswer: "C",
                IncorrectAnswer: "The correct answer is ​C:​ if published to your institution’s website, it will appear on your profile. If your Admin chooses, they can also publish it to social media. It will not appear solely in the app if it has been published by an Admin"
            },
        ],
    },
    {
        index: 4,
        title: "What is FAQ?",
        video_title: "WHAT IS FAQ?",
        video: "quiz4.mp4",
        quiz: [
            {
                index: 1,
                question: "What’s the benefit of FAQ?",
                answers: [
                    {
                        type: "A",
                        content: "Makes it easier for prospective students to find out information they need"
                    },
                    {
                        type: "B",
                        content: "Lets prospective students ask questions directly"
                    },
                    {
                        type: "C",
                        content: "Lets Ambassadors ask prospective students questions"
                    }
                ],
                RightAnswer: "A",
                IncorrectAnswer: "The correct answer is ​A: ​FAQ enables prospective students to find specific information quickly "
            },
            {
                index: 2,
                question: "How do you edit an answer you’ve already sent to an Admin?",
                answers: [
                    {
                        type: "A",
                        content: "Message the Admin asking them to edit it"
                    },
                    {
                        type: "B",
                        content: "Send an additional answer"
                    },
                    {
                        type: "C",
                        content: "Find your answer on the website and edit it yourself"
                    }
                ],
                RightAnswer: "B",
                IncorrectAnswer: "The correct answer is ​B​: if you want to edit your FAQ answer, or add any additional detail, send an additional answer in the FAQ conversation."
            },
            {
                index: 3,
                question: "What happens after you’ve submitted your FAQ answer?",
                answers: [
                    {
                        type: "A",
                        content: "It’s published directly to your institution’s website"
                    },
                    {
                        type: "B",
                        content: "It’s sent to prospective students’ inboxes"
                    },
                    {
                        type: "C",
                        content: "Your Admin checks it for spelling, then publishes it"
                    }
                ],
                RightAnswer: "C",
                IncorrectAnswer: "The correct answer is ​C​: Admins will check the FAQ answers for spelling or information inaccuracies before publishing it to the website"
            },
        ],
    }
];

export default quizQuestions;
