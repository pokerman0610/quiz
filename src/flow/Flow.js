import React, { Component } from 'react';
import quizQuestions from '../api/quizQuestions';
import Quiz from '../components/Quiz';
import QuizTitle from '../components/QuizTitle';
import Result from '../components/Result';
import EndPage from '../components/EndPage';
import MobileVideo from './MobileVideo';

import logo from '../svg/logo.svg';
import './Flow.css';
import {CSSTransitionGroup} from "react-transition-group";
import QuizSeparatePage from "../components/QuizSeparatePage";

class App extends Component {
    constructor(props) {
        super(props);
        this.remove_indexs = [];

        switch(parseInt(props.flow_number)){
            case 1:
                this.remove_indexs=[4];break;
            case 2:
                this.remove_indexs=[3];break;
            case 3:
                this.remove_indexs=[3, 4];break;
            case 4:
                this.remove_indexs=[2];break;
            case 5:
                this.remove_indexs=[2, 4];break;
            case 6:
                this.remove_indexs=[2, 3];
        }
        for (var i = this.remove_indexs.length; i > 0; i--) {
            quizQuestions.splice(this.remove_indexs[i - 1] - 1, 1);
        }

        this.state = {
            quizId: 1,
            counter: 0,
            questionId: 1,
            question: '',
            answerOptions: [],
            answer: '',
            answersCount: {},
            questionResult: '',
            incorrectAnswer: '',
            flowState: 1, // 1: show Title / 2: show Video / 3: show Questions / 4. Quiz Separate Page / 5. End Page
            result: '',
            access_code:props.access_code
        };

        this.handleAnswerSelected = this.handleAnswerSelected.bind(this);
        this.handleNextButtonClick = this.handleNextButtonClick.bind(this);
        this.handlePrevButtonClick = this.handlePrevButtonClick.bind(this);
        this.StartQuestion = this.StartQuestion.bind(this);
    }

    componentDidMount() {
        if(this.state.flowState === 1){
            setTimeout(() => this.showVideo(), 1400);
        }
        // const shuffledAnswerOptions = quizQuestions[this.state.quizId - 1].quiz.map(question =>
        //     this.shuffleArray(question.answers)
        // );
        this.setState({
            question: quizQuestions[0].quiz[0].question,
            answerOptions: quizQuestions[this.state.quizId - 1].quiz[0].answers
            // answerOptions: shuffledAnswerOptions[0]
        });
    }

    shuffleArray(array) {
        var currentIndex = array.length,
            temporaryValue,
            randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    handleAnswerSelected(event) {
        this.setUserAnswer(event.currentTarget.value);
        this.showQuestionResponse(event.currentTarget.value);
    }
    handleNextButtonClick(){
        if(this.state.quizId === quizQuestions.length &&
           this.state.questionId === quizQuestions[this.state.quizId - 1].quiz.length) {
            if(this.state.flowState === 3){
                setTimeout(() =>this.setState({ flowState: 4}), 500);
            }else{
                setTimeout(() =>this.setState({ flowState: 5}), 650);
            }
            return;
        }

        if (this.state.questionId < quizQuestions[this.state.quizId - 1].quiz.length) {
            setTimeout(() => this.setNextQuestion(), 300);
        } else {
            if(this.state.flowState === 3){
                setTimeout(() =>this.setState({ flowState: 4}), 500);
            }else if(this.state.flowState === 4) {
                setTimeout(() => this.setState({
                    quizId: this.state.quizId + 1,
                    counter: 0,
                    questionId: 1,
                    answerOptions: quizQuestions[this.state.quizId].quiz[0].answers,
                    question: quizQuestions[this.state.quizId].quiz[0].question,
                    answer: '',
                    answersCount: {},
                    questionResult: '',
                    incorrectAnswer: '',
                    flowState: 1, // 1: show Title / 2: show Video / 3: show Questions / 4. Quiz Separate Page / 5. End Page
                    result: ''
                }), 700);
                setTimeout(() => this.showVideo(), 1700);
            }
        }
    }
    handlePrevButtonClick(){
        const counter = this.state.counter - 1;
        const questionId = this.state.questionId - 1;

        this.setState({
            counter: counter,
            questionId: questionId,
            question: quizQuestions[this.state.quizId - 1].quiz[counter].question,
            answerOptions: quizQuestions[this.state.quizId - 1].quiz[counter].answers,
            questionResult:'',
            incorrectAnswer:'',
            answer: ''
        });
    }
    setNextQuestion() {
        const counter = this.state.counter + 1;
        const questionId = this.state.questionId + 1;

        this.setState({
            counter: counter,
            questionId: questionId,
            question: quizQuestions[this.state.quizId - 1].quiz[counter].question,
            answerOptions: quizQuestions[this.state.quizId - 1].quiz[counter].answers,
            questionResult:'',
            incorrectAnswer:'',
            answer: ''
        });
    }
    setUserAnswer(answer) {
        this.setState((state, props) => ({
            answersCount: {
                ...state.answersCount,
                [answer]: (state.answersCount[answer] || 0) + 1
            },
            answer: answer
        }));
    }
    showVideo(){
        this.setState({
            flowState: 2,
        });
    }
    StartQuestion(){
        // console.log("StartQuestion===================");

        setTimeout(() => this.setState({flowState: 3}), 750);
    }
    showQuestionResponse(answer){
        if(answer === quizQuestions[this.state.quizId - 1].quiz[this.state.counter].RightAnswer) {
            this.setState({
                questionResult: "Correct!",
                incorrectAnswer: ''
            });
        }else{
            this.setState({
                questionResult: "Incorrect!",
                incorrectAnswer:quizQuestions[this.state.quizId - 1].quiz[this.state.counter].IncorrectAnswer
            });
        }
    }

    getResults() {
        const answersCount = this.state.answersCount;
        const answersCountKeys = Object.keys(answersCount);
        const answersCountValues = answersCountKeys.map(key => answersCount[key]);
        const maxAnswerCount = Math.max.apply(null, answersCountValues);

        return answersCountKeys.filter(key => answersCount[key] === maxAnswerCount);
    }

    setResults(result) {
        if (result.length === 1) {
            this.setState({ result: result[0] });
        } else {
            this.setState({ result: 'Undetermined' });
        }
    }

    renderQuiz() {
        return (
            <Quiz
                answer={this.state.answer}
                answerOptions={this.state.answerOptions}
                questionId={this.state.questionId}
                question={this.state.question}
                questionTotal={quizQuestions[this.state.quizId - 1].quiz.length}
                questionResult={this.state.questionResult}
                incorrectAnswer={this.state.incorrectAnswer}
                onAnswerSelected={this.handleAnswerSelected}
                onPrevButtonClick={this.handlePrevButtonClick}
                onNextButtonClick={this.handleNextButtonClick}
                StartQuestion={this.StartQuestion}
                video_url=""
            />
        );
    }

    renderResult() {
        return <Result quizResult={this.state.result} />;
    }

    render() {
        return (
            <div className="App">
                {(() => {
                    if(this.state.flowState === 1){  // 1: show Title
                        return(<QuizTitle Title={quizQuestions[this.state.quizId - 1].video_title}></QuizTitle>)
                    }else if(this.state.flowState === 2){ //2: show Video
                        return(<MobileVideo StartQuestion={this.StartQuestion}
                                            video_url={quizQuestions[this.state.quizId - 1].video}></MobileVideo>)
                    }else if(this.state.flowState === 3){ //3: show Questions
                        return <CSSTransitionGroup
                            component="div"
                            transitionName="fade"
                            transitionEnterTimeout={800}
                            transitionLeaveTimeout={500}
                            transitionAppear
                            transitionAppearTimeout={500}
                        >
                            <div className="App-header">
                                <img src={logo} className="App-logo" alt="logo" />
                                <h2>{quizQuestions[this.state.quizId - 1].title}</h2>
                            </div>
                            {this.renderQuiz()}
                        </CSSTransitionGroup>
                    }else if(this.state.flowState === 4){ //4: Quiz Separate Page
                        return <CSSTransitionGroup
                            component="div"
                            transitionName="fade"
                            transitionEnterTimeout={800}
                            transitionLeaveTimeout={500}
                            transitionAppear
                            transitionAppearTimeout={500}
                        >
                            <div className="App-header">
                                <img src={logo} className="App-logo" alt="logo" />
                                <h2>{quizQuestions[this.state.quizId - 1].title}</h2>
                            </div>
                            <QuizSeparatePage
                                onNextButtonClick={this.handleNextButtonClick}
                                quizTitle={quizQuestions[this.state.quizId - 1].title}
                            />
                        </CSSTransitionGroup>
                    }else if(this.state.flowState === 5){//5: EndPage
                        return(<EndPage accesscode={this.state.access_code}></EndPage>)
                    }
                })()}
            </div>
        );
    }
}

export default App;
