import React from 'react'
import {useGlobal} from 'reactn'
import { IntroWrapper, Header, IntroText, Button } from '../components/styled.js'
import {NavLink} from 'react-router-dom'
import logo from "../logo.svg";
import './intro.sass'
function Intro() {

  const [Flow] = useGlobal('flow')

  const nextPage = "/Flow"

  return (
      <div className='wrapper'>
          <IntroWrapper>
            <header className='controls'>
                <div className='logo'>
                    <img src={logo} alt='TAP logo'/>
                </div>
                <div className='links'>
                    {/* <NavLink exact to="/">Link</NavLink> */}
                </div>
            </header>
            <Header>Welcome to the<br></br>Ambassador Training Centre</Header>
            <IntroText>This training will take 10 minutes, after which you’ll be fully informed about the platform.<br></br>After each video, you will answer a few multiple-choice questions.
            </IntroText>
            <NavLink exact to={nextPage}><Button>Get started</Button></NavLink>
          </IntroWrapper>
      </div>)
}

export default Intro;
