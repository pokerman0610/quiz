import React from 'react'
import ReactPlayer from 'react-player'
import { isMobile } from 'react-device-detect'

import Video1 from '../video/quiz1.mp4'
import Video2 from '../video/quiz2.mp4'
import Video3 from '../video/quiz3.mp4'
import Video4 from '../video/quiz4.mp4'
import PropTypes from "prop-types";
import Quiz from "../components/Quiz";
import {CSSTransitionGroup} from "react-transition-group";

function MobileVideo(props) {
  function getVideoSource(url){
      if(url === "quiz1.mp4")
          return Video1;
      else if(url === "quiz2.mp4")
          return Video2;
      else if(url === "quiz3.mp4")
          return Video3;
      else if(url === "quiz4.mp4")
          return Video4;
  }
  return (
      <CSSTransitionGroup
          component="div"
          transitionName="fade"
          transitionEnterTimeout={800}
          transitionLeaveTimeout={500}
          transitionAppear
          transitionAppearTimeout={500}
      >
        <div className='player-wrapper'>
          <ReactPlayer className='react-player' onEnded={props.StartQuestion} url={getVideoSource(props.video_url)} width='100%' height='100%' playing={ isMobile ? false : true} controls muted/>
        </div>
      </CSSTransitionGroup>);
}
Quiz.propTypes = {
    StartQuestion: PropTypes.func.isRequired,
    video_url: PropTypes.string.isRequired,
};

export default MobileVideo;
