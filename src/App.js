import React from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import queryString from 'query-string'
import CryptoJS from 'crypto-js'
import 'normalize.css'

import Intro from './flow/intro.js'
import Flow from './flow/Flow.js'

export default function App() {

const values = queryString.parse(window.location.search)
const unikey = 'NcRfUjXn2r5u8x/A%D*G-KaPdSgVkYp3'
var flow_number = 6;
var access_code = "custom";

if (values.flow) {
//---------------- CryptoJs ----------------

    const unidata = CryptoJS.AES.decrypt(atob(values.flow), unikey).toString(CryptoJS.enc.Utf8)

    access_code = unidata.substring(0, 6)
    flow_number = unidata.substring(6)
    console.log('access code: ' + access_code + ', flow number: ' + flow_number)
}

return (
  <BrowserRouter basename={process.env.PUBLIC_URL}>
    <Switch>
        <Route exact path="/" component={() => <Intro/>}/>
        <Route exact path="/Flow" component={() => <Flow flow_number={flow_number} access_code={access_code}/>}/>
        <Route render={() => <p>Not Found</p>} />
    </Switch>
</BrowserRouter>
);
}
